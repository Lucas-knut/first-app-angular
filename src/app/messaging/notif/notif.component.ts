import { MessagesService } from './../services/messages.service';
import { Component, OnInit } from '@angular/core';
import { Message } from '../models/message';

@Component({
  selector: 'app-notif',
  templateUrl: './notif.component.html',
  styleUrls: ['./notif.component.css']
})
export class NotifComponent implements OnInit {

  messages!: Message[]
  count: number = 0

  constructor(
    private messagesService: MessagesService,
  ) { }

  ngOnInit(): void {
    this.messagesService
    .subject
    .subscribe(messages => {
      this.count = messages.length
      console.log(this.messages);

    })
  }

}
