import { Message } from './../models/message';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {

  subject: BehaviorSubject<Message[]>
  private messages = [] as Message[]

  constructor(
    private http: HttpClient,
    private router: Router) {
      this.subject = new BehaviorSubject([] as Message[])
  }

  async load() {
    let messages = await this.http.get("").toPromise()
  }

  getAll(): Promise<Message[]> {
    return this.http
    .get<Message[]>("http://localhost:3000/messages")
    .toPromise();
  }

  getById(id: number): Message | undefined{
    // return this.messages.find(message => message.id = id);
    return;
  }

  create(message: Message): void {
    // this.messages.push(message);
    this.http
    .post("http://localhost:3000/messages", message)
    .toPromise()
    .then(datas => {
      // console.log(datas);
      this.reaload()
      this.router.navigate(['/inbox'])
    })
    .catch(reason => {
      console.log(reason);
    })
  }

  // Retourne l'observable de HttpCLient
  getObservable(): Observable<Message[]> {
    return this.http
    .get<Message[]>("http://localhost/3000/messages")
  }

  reaload(): void {
    this.http
    .get<Message[]>("")
    .toPromise()
    .then(messages => {
      this.subject.next(messages)
    })
  }

}
