export interface Message {
  id?: Number,
  from?: String,
  to?: String,
  subject?: String,
  body?: String,
  read?: Boolean
}
