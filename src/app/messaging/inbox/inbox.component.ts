import { MessagesService } from './../services/messages.service';
import { Component, Input, OnInit } from '@angular/core';
import { Message } from '../models/message';

@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.css']
})
export class InboxComponent implements OnInit {

  @Input() messages!: Message[]

  @Input() count: number = 0

  constructor(
    private messagesService: MessagesService,
    ) {}

  ngOnInit(): void {
    // this.messagesService.getAll()
    // .then(ms => {
    //   this.messages = ms
    // })
    // .catch(reason => console.log(reason));

    // Gestion du nombre de messages
    this.messagesService
    .subject
    .subscribe(messages => {
      this.messages = messages
    })
    this.messagesService.reaload()
  }


  filter(event: Event) {
    let input = event.target as HTMLInputElement;
    this.messages = this.messages.filter(m => m.read === input.checked)

    console.log(event)
  }

}
