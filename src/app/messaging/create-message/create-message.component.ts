import { MessagesService } from './../services/messages.service';
import { Message } from './../models/message';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-create-message',
  templateUrl: './create-message.component.html',
  styleUrls: ['./create-message.component.css']
})
export class CreateMessageComponent implements OnInit {

  message: Message = {
    id: 1,
    from: "",
    to: "",
    subject: "",
    body: "",
    read: false
  }

  constructor(
    private messagesService: MessagesService
  ) { }

  ngOnInit(): void {
  }

  send() {
    this.messagesService.create(this.message);
  }
}
