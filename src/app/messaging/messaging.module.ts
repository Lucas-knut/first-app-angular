import { NotifComponent } from './notif/notif.component';
import { CreateMessageComponent } from './create-message/create-message.component';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InboxComponent } from './inbox/inbox.component';
import { CardModule } from 'primeng/card';
import { HttpClientModule } from '@angular/common/http';



@NgModule({
  declarations: [
    CreateMessageComponent,
    InboxComponent,
    NotifComponent
  ],
  imports: [
    CommonModule,
    CardModule,
    FormsModule,
    HttpClientModule
  ],
  exports: [
    InboxComponent,
    CreateMessageComponent,
    NotifComponent
  ]
})
export class MessagingModule { }
