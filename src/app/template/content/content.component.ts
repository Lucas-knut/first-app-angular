import { Component, OnInit } from '@angular/core';
import { Message } from '../../messaging/models/message';


@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {

  m: Message[] = []

  constructor() { }

  ngOnInit(): void {
    this.m = [
      {
        id: 1,
        from: "Charlie",
        to: "Victor",
        subject: "Restaurant",
        body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.",
        read: false
      },
      {
        id: 2,
        from: "Jean",
        to: "Carla",
        subject: "Music",
        body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.",
        read: true
      },
      {
        id: 3,
        from: "Bob",
        to: "Jessie",
        subject: "Logo done",
        body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.",
        read: false
      },
      {
        id: 4,
        from: "Philipe",
        to: "Maxime",
        subject: "Flyer",
        body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.",
        read: true
      },
      {
        id: 5,
        from: "Chloé",
        to: "Bernard",
        subject: "Facture",
        body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.",
        read: false
      }
    ]
  }

}
