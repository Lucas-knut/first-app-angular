import { NotifComponent } from './messaging/notif/notif.component';
import { CreateMessageComponent } from './messaging/create-message/create-message.component';
import { RouterModule, Routes } from '@angular/router';
import { InboxComponent } from './messaging/inbox/inbox.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


const Routes = [
{
  path: "inbox", component: InboxComponent,
},
{
  path: "create-message", component: CreateMessageComponent,
},
{
  path: "notif", component: NotifComponent,
}
] as Routes;


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(Routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
